#!/usr/bin/env python3

#
# Poetry is amazing, but it doesn't provide any hooks for you to do simple
# things like (for example) modify a README file before pushing it to PyPI.
# So, if you're like me and have a repo with a bunch of relative URLs in it
# pointing to other parts of the repo (diagrams, video demos, etc), then that
# README will look broken on PyPI unless you patch the file before publishing
# it.  That's what this does.
#
# Where you once called `poetry build`, instead call this script and it will:
#
# 1. Go through your README and rewrite all of your relative URLs to be
#    absolute ones pointing to the `master` repo.
# 2. Run `poetry build`
# 3. Restore your README.md file back to what it was.
#
# NOTE: This was written for GitLab, so if you're hoping to get this to work
# NOTE: with GitHub or some other service, you'll have to tweak it.
#

import re
import sys

from pathlib import Path
from subprocess import check_call

import git


class CommandError(Exception):
    pass


class Command:

    LINK_FIXER = re.compile(r"]\(([^:)]+)\)")
    CHANGELOG_FIXER = re.compile(r"^#", re.MULTILINE)

    def __init__(self):
        self.remote = None
        self.git = git.Repo(".")

    def check_files_exist(self):
        for f in ("pyproject.toml", "README.md"):
            if not Path(f).exists():
                raise CommandError(f"{f} not found.  This isn't happening.")

    def check_for_uncommitted_changes(self):
        if self.git.is_dirty(untracked_files=True):
            raise CommandError("The project isn't ready for building yet.")

    def check_is_master(self):
        if not self.git.active_branch.name == "master":
            raise CommandError("You should be on the master branch.")

    def check_is_gitlab(self):
        for remote in self.git.remotes:
            if not remote.name == "origin":
                continue
            for url in remote.urls:
                if "gitlab" not in url:
                    continue
                url = url.replace(":", "/").replace(".git", "")
                self.remote = f"https://{url}/-/raw/master/"
                return

        raise CommandError(
            "This doesn't appear to be a Gitlab project.  You're on your own."
        )

    def patch_readme(self):

        readme = Path("README.md")
        changelog = Path("CHANGES.md")

        with readme.open() as r:
            main = self.LINK_FIXER.sub(f"]({self.remote}\\1)", r.read())

        supplementary = ""
        if changelog.exists():
            with changelog.open() as r:
                supplementary = self.CHANGELOG_FIXER.sub("##", r.read())

        with readme.open("w") as w:
            w.write(main)
            if supplementary:
                w.write("\n\n")
                w.write(supplementary)

    @staticmethod
    def build_and_publish() -> None:
        check_call(["poetry", "build"])
        check_call(["poetry", "publish"])

    def restore_readme(self) -> None:
        self.git.git.checkout("README.md")

    def __call__(self, *args, **kwargs):

        try:
            self.check_files_exist()
            self.check_for_uncommitted_changes()
            self.check_is_master()
            self.check_is_gitlab()
        except CommandError as e:
            sys.stderr.write(f"\n  {e}\n\n")
            sys.exit(1)

        self.patch_readme()
        self.build_and_publish()
        self.restore_readme()


if __name__ == "__main__":
    Command()()
