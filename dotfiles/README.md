# dotfiles

These are an elaborate set of scripts I've collected over the years to
customise my shell experience, specifically the prompt, along with a bunch of
handy functions & aliases.

This is also the home of some motd scripts that I derrived from [yboetz's excellent work](https://github.com/yboetz/motd).

