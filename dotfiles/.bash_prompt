#! /bin/bash

function setprompt {

  ##
  ## Set some colours for easy reference.
  ##


  local width=$(tput cols)

  local good="\\033[38;5;022m"
  local warn="\\033[38;5;214m"
  local bad="\\033[38;5;124m"

  local        black="\[\033[0;30m\]"
  local        white="\[\033[1;37m\]"
  local         blue="\[\033[0;34m\]"
  local        green="\[\033[0;32m\]"
  local         cyan="\[\033[0;36m\]"
  local          red="\[\033[0;31m\]"
  local       yellow="\[\033[1;33m\]"
  local       purple="\[\033[0;35m\]"
  local        brown="\[\033[0;33m\]"
  local         grey="\[\033[1;30m\]"
  local   light_grey="\[\033[0;37m\]"
  local   light_blue="\[\033[1;34m\]"
  local  light_green="\[\033[1;32m\]"
  local   light_cyan="\[\033[1;36m\]"
  local    light_red="\[\033[1;31m\]"
  local light_purple="\[\033[1;35m\]"
  local    no_colour="\[\033[0m\]"


  # set date colour based on $PROMPT_HOST_COLOUR or use a default

  local host_colour=${grey}
  if [[ "${PROMPT_HOST_COLOUR}" != "" ]]; then
    host_colour=${PROMPT_HOST_COLOUR}
  fi

  local load_high=4
  if [[ "${PROMPT_LOAD_HIGH}" != "" ]]; then
    load_high=${PROMPT_LOAD_HIGH}
  fi

  local load_low=2
  if [[ "${PROMPT_LOAD_LOW}" != "" ]]; then
    load_low=${PROMPT_LOAD_LOW}
  fi

  ##
  ## Get the current system load (1 min, 5 min, 15 min)
  ##

  local load_1=$(uptime | sed -e 's/^.*: \([0-9.]\+\), \([0-9.]\+\), \([0-9.]\+\).*$/\1/')
  local load_5=$(uptime | sed -e 's/^.*: \([0-9.]\+\), \([0-9.]\+\), \([0-9.]\+\).*$/\2/')
  local load_15=$(uptime | sed -e 's/^.*: \([0-9.]\+\), \([0-9.]\+\), \([0-9.]\+\).*$/\3/')

  local load_1_int=$(echo ${load_1} | sed -e 's/^\([0-9]\+\).*/\1/')
  local load_5_int=$(echo ${load_5} | sed -e 's/^\([0-9]\+\).*/\1/')
  local load_15_int=$(echo ${load_15} | sed -e 's/^\([0-9]\+\).*/\1/')

  local load_1_display=$(echo ${load_1} | awk '{printf "%0.1f", $1}' | sed -e 's/\.//')
  local load_5_display=$(echo ${load_5} | awk '{printf "%0.1f", $1}' | sed -e 's/\.//')
  local load_15_display=$(echo ${load_15} | awk '{printf "%0.1f", $1}' | sed -e 's/\.//')

  if [ ${load_1_int} -lt ${load_low} ]; then
    load_1c="${good}${load_1_display}${no_colour}"
  elif [ ${load_1_int} -lt ${load_high} ]; then
    load_1c="${warn}${load_1_display}${no_colour}"
  else
    load_1c="${bad}${load_1_display}${no_colour}"
  fi

  if [ ${load_5_int} -lt ${load_low} ]; then
    load_5c="${good}${load_5_display}${no_colour}"
  elif [ ${load_5_int} -lt ${load_high} ]; then
    load_5c="${warn}${load_5_display}${no_colour}"
  else
    load_5c="${bad}${load_5_display}${no_colour}"
  fi

  if [ ${load_15_int} -lt ${load_low} ]; then
    load_15c="${good}${load_15_display}${no_colour}"
  elif [ ${load_15_int} -lt ${load_high} ]; then
    load_15c="${warn}${load_15_display}${no_colour}"
  else
    load_15c="${bad}${load_15_display}${no_colour}"
  fi


  ##
  ## Check on the partition usage.
  ## By default we always capture the capacity of root (/), but you can add
  ## more to the list of mounts checked by declaring PROMPT_DF and assigning it
  ## a value in the following format:
  ##
  ##   <letter>:<path>
  ##
  ## For example:
  ##
  ## PROMPT_DF="x:/path/something;y:/path/to/something/else"
  ##

  local df=$(df -P /| awk '$6 ~ "^/$" { print $5 }' | sed -e 's/\(.\+\)./\1/')

  if [[ ${df} -lt 70 ]]; then
    df_all="${good}${df}%${no_colour}"
  elif [[ ${df} -lt 90 ]]; then
    df_all="${warn}${df}%${no_colour}"
  else
    df_all="${bad}${df}%${no_colour}"
  fi

  if [[ "${PROMPT_DF}" != "" ]]; then

    local mounts=""; IFS=';'; mounts=(${PROMPT_DF}); unset IFS;

    for name in ${mounts}; do

      local letter=$(echo "${name}" | sed -e 's/:.*//')
      local path=$(echo "${name}" | sed -e 's/.*://')
      local df_tmp=$(df -P ${path} | awk '$6 ~ "^'${path}'$" { print $5 }' | sed -e 's/\(.\+\)./\1/')

      if [[ ${df_tmp} -lt 70 ]]; then
        df_tmp="${good}${letter}:${df_tmp}%${no_colour}"
      elif [[ ${df_tmp} -lt 90 ]]; then
        df_tmp="${warn}${letter}:${df_tmp}%${no_colour}"
      else
        df_tmp="${bad}${letter}:${df_tmp}%${no_colour}"
      fi

      df_all="${df_all} ${df_tmp}"

    done
  fi

  # Account for a variable number of df choices
  local df_line=""
  for df in ${df_all}; do
    df_line=" ${df_line}${df}${no_colour}"
  done

  ##
  ## Network Connections
  ##

  local connections=$(netstat -nut | grep -E '^tcp|udp' | wc -l)

  local vpn=""
  local tailscale_status=$(tailscale status >& /dev/null; echo $?)
  local mullvad_status=1  # TODO
  local openvpn_status=1  # TODO

  if [[ ${tailscale_status} -eq 0 ]]; then
    vpn="t"
  elif [[ ${mullvad_status} -eq 0 ]]; then
    vpn="m"
  elif [[ ${openvpn_status} -eq 0 ]]; then
    vpn="o"
  fi

  if [ ${connections} -lt 25 ]; then
    connections="️ ${good}${vpn}${connections}${no_colour}"
  elif [ ${connections} -lt 50 ]; then
    connections=" ${warn}${vpn}${connections}${no_colour}"
  else
    connections=" ${bad}${vpn}${connections}${no_colour}"
  fi


  ##
  ## Battery capacity
  ##

  local battery=""

  if [[ -e /sys/class/power_supply/BAT0/capacity ]]; then

    battery=$(</sys/class/power_supply/BAT0/capacity)

    if [[ ${battery} -lt 25 ]]; then
      battery="${bad}${battery}${no_colour}"
    elif [[ ${battery} -lt 50 ]]; then
      battery="${warn}${battery}${no_colour}"
    else
      battery="${good}${battery}${no_colour}"
    fi

  fi

  local battery_line=""
  if [[ "${battery}" != "" ]]; then
    battery_line=" ${no_colour}${battery}${no_colour}"
  fi

  ##
  ## Git
  ##

  local branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
  if [[ ${branch} ]]; then
    commit=$(git rev-parse --short HEAD 2> /dev/null)
    branch=" ${green}${branch}:${commit}"
  fi

  ##
  ## Python
  ##

  local virtualenv=${VIRTUAL_ENV}
  if [[ ${virtualenv} ]]; then
    virtualenv=$(echo ${virtualenv} | sed -E 's#^.*?/([^/]+)/.venv#\1#')
    virtualenv=" ${blue}${virtualenv}"
  fi

  ##
  ## Set the username string.
  ##

  if [[ $UID -eq 0 ]]; then
    local user="${bad}\u${white}@${host_colour}\h"
  else
    local user="${host_colour}\u@\h"
  fi

  ##
  ## Networking
  ##

  local networking="${load_1c}|${load_5c}|${load_15c}"

  ##
  ## Date & actual prompt
  ##
  local date_and_prompt="${cyan}`date '+%F %T'`${white} \\$ ${no_colour}"


PS1="${user} \
${networking}\
${df_line}\
${connections}\
${battery_line}\
${virtualenv}\
${branch}${no_colour} ${cyan}\w${no_colour}\
\n\
${date_and_prompt}"

}

export PROMPT_COMMAND=setprompt
