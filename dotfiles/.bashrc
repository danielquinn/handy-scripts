# Exit the process if we're not interactive
[[ $- != *i* ]] && return


# These all courtesy of https://github.com/VivaCaligula/DOTFILES

# Ignore duplicate commands, & ignore commands starting with a space.
export HISTCONTROL="erasedups:ignorespace"

# Infinite history
export HISTFILESIZE=
export HISTSIZE=

# Append to the history instead of overwriting (good for multiple connections).
shopt -s histappend


# Colourise the prompt according to the available CPU cores

PROMPT_LOAD_HIGH=$(nproc)
PROMPT_LOAD_LOW=$(($(nproc)/2))


# Look for and load-if-exists stuff

if [[ -f ${HOME}/.bash_aliases ]]; then
  . ${HOME}/.bash_aliases
fi

if [[ -f ${HOME}/.bash_prompt ]]; then
  . ${HOME}/.bash_prompt
fi

export EDITOR="/usr/bin/vim"
export GOPATH="${HOME}/.cache/go"
export PATH="${PATH}:${GOPATH}/bin:${HOME}/.local/bin"
export XDG_DATA_DIRS="${XDG_DATA_DIRS}:${HOME}/.local/share"
export BUILDKIT_PROGRESS=plain

# SSH agent magic: https://stackoverflow.com/questions/18880024/start-ssh-agent-on-login
# Run `systemctl --user start ssh-agent` to make this go, and remember
# to include `AddKeysToAgent yes` to ${HOME}/.ssh/config
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

if [[ -d ~/.motd.d ]]; then
  for f in ${HOME}/.motd.d/*; do MOTD_MOUNTPOINTS=${MOTD_MOUNTPOINTS} MOTD_SERVICES=${MOTD_SERVICES} ${f}; done
fi

if [[ "$(command -v navi)" != "" ]]; then
  eval "$(navi widget bash)"
fi

if [[ "$(command -v db)" != "" ]]; then
  source <(db completion bash)
fi

if [[ "$(command -v kubectl)" != "" ]]; then
  source <(kubectl completion bash)
  complete -F __start_kubectl k
fi

#if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
#  source /etc/profile.d/vte.sh
#fi

if [[ -e "${HOME}/.secrets" ]]; then
  . ${HOME}/.secrets
fi
