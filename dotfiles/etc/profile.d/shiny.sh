#!/bin/sh

BLACK="\033[0;30m"
BLUE="\033[0;34m"
GREEN="\033[0;32m"
CYAN="\033[0;36m"
RED="\033[0;31m"
PURPLE="\033[0;35m"
BROWN="\033[0;33m"
LIGHT_GREY="\033[0;37m"
DARK_GREY="\033[1;30m"
LIGHT_BLUE="\033[1;34m"
LIGHT_GREEN="\033[1;32m"
LIGHT_CYAN="\033[1;36m"
LIGHT_RED="\033[1;31m"
LIGHT_PURPLE="\033[1;35m"
YELLOW="\033[1;33m"
WHITE="\033[1;37m"
NO_COLOUR="\033[0m"
QUOTE='"'

HOST_COLOUR=${PURPLE}
if [[ "${UID}" = "0" ]]; then
    HOST_COLOUR=${RED}
fi

export PS1="\
${WHITE}[${HOST_COLOUR}\u${WHITE}@${HOST_COLOUR}\h${WHITE}:${BROWN}\w$WHITE]${NO_COLOUR}\
\$(git rev-parse --abbrev-ref HEAD 2> /dev/null | awk '{printf \"${WHITE}[${NO_COLOUR}${GREEN}%s${WHITE}]${NO_COLOUR}\", \$1}' 2> /dev/null)\n\
${WHITE}(${CYAN}\$(date '+%F %T')${WHITE}) \\$ ${NO_COLOUR}"

# Internal aliases.  It's uncommon that we use these, but handy in the few
# cases when we're poking around a running container.

alias ll="ls -l"
alias lh="ls -lh"
alias la="ls -la"
