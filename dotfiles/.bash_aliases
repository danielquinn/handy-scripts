#!/bin/bash

# ls

alias ls="ls --color --group-directories-first"
alias ll="ls -l"
alias lh="ls -lh"
alias la="ls -la"


# cd

alias ..='cd ../'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../../'


# cat
alias cat="bat"


# grep

alias grep="grep -I --colour=auto --exclude=node_modules"
alias cfgrep='grep -Ev "^[[:space:]]*($|#)"'


# git

git config --global alias.l "log --stat"
git config --global alias.ll "log --graph --decorate --abbrev-commit --pretty=medium --branches --remotes"
function pr {

  default_branch=$(git config init.defaultbranch)
  current_branch=$(git rev-parse --abbrev-ref HEAD)

  read -rp "Title: " title
  read -rp "Is this a draft? " is_draft

  draft=""
  if [[ "${is_draft}" = "y" ]]; then
    draft="--draft"
  fi

  gh pr create --base "${default_branch}" --head "${current_branch}" --title "${title}" "${draft}"

}


# Django

alias sp="./manage.py shell_plus"
alias ds="./manage.py dbshell"
alias rs="DJANGO_LOG_LEVEL=INFO ./manage.py runserver"
alias rp="DJANGO_LOG_LEVEL=INFO ./manage.py runserver_plus"


# Kubernetes

alias k="kubectl"
alias kn="kubectl config set-context --current --namespace"
alias kctx="kubectl config get-contexts"
alias ksw="kubectl config use-context"
alias kgp="kubectl get pods"
alias wkgp="watch kubectl get pods"
alias stern="stern --exclude-container='istio-proxy' --timestamps"
alias kevents='kubectl get events --sort-by=".metadata.creationTimestamp"'

# K3s
alias garbage-collect-k3s="sudo k3s crictl rmi --prune"


# Everything Else

alias which="command -v"
alias doc="docker compose"
alias dlf="docker compose logs --follow"
alias docker-nuke="docker system prune --all --volumes --force"
alias please='sudo $(fc -n -l -1)'
alias yt="yt-dlp -o '%(title)s-%(id)s.%(ext)s' "
alias servethis="python3 -m http.server"
alias whatismyip="curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//'"
alias boss='while [[ : ]]; do head -n 100 /dev/urandom; sleep .1; done | hexdump -C | grep "ca fe"'
alias mount='mount |column -t'
alias mine="sudo chown -R $(id -u):$(id -g) "

# Thanks to Cory Doctorow for this one: https://twitter.com/doctorow/status/866774675317305344?s=09
alias subvert-tether="sudo sysctl net.ipv4.ip_default_ttl=65"

#
# Degrade our internet connection for testing
#
function comcast {
  local device=$(netstat -rn | grep -E "^0\.0\.0\.0" | awk '{print $8}')
  if [[ "${1}" = "fucked" ]]; then
    echo "Welcome to Australia!"
    sudo tc qdisc add dev ${device} root netem delay 100ms
  else
    echo "Removing network crippling"
    sudo tc qdisc delete dev ${device} root
  fi
}


function xtar {
  local source=${1}
  local destination=${2}
  if [[ ! ${source} || ! ${destination} ]]; then
    echo "Usage: xtar <source> <destination>"
    return
  fi
  tar -c --to-stdout ${source} | xz -9 --threads=$(nproc) --to-stdout > ${destination}
}


function vpn-check {
  local ip=$(curl -s 'https://am.i.mullvad.net/')
  local country=$(curl -s "https://stat.ripe.net/data/geoloc/data.json?resource=${ip}" | jq .data.located_resources[0].locations[0].country | sed -e 's/"//g')
  if [[ "${country}" = "UK" ]]; then
    echo "Nope"
    return 1
  else
    echo "Looks like we're in ${country}"
    return 0
  fi
}


#
# Move up through the directory tree looking for pyproject.toml, and if we
# find one, setup our virtual environment.
#
function ev {

  local exists="0"
  local path="."
  while [[ ${path} != "/" ]]; do
    local found=$(find "${path}" -maxdepth 1 -mindepth 1 -name pyproject.toml "${@}")
    if [[ ${found} ]]; then
      exists="1"
      echo "pyproject.toml found: ${path}"
      break
    fi
    path="$(readlink -f "${path}/..")"
  done

  if [[ "${exists}" = "1" ]]; then
    poetry shell
  else
    echo "No pyproject.toml could be found"
  fi

}


#
# Rebase the current branch off of `master` or the specified branch
#
function rebase {

  local branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
  if [[ "${branch}" = "" ]]; then
    echo "You need to be inside a git repo"
    return 1
  fi

  if [[ ${branch} == "develop" || ${branch} == "master" ]]; then
    echo "Don't screw with this branch"
    return 1
  fi

  git update-index --refresh
  git diff-index --quiet HEAD --
  if [[ "$?" == "1" ]]; then
    echo "You have uncommitted changes.  Exiting."
    return 1
  fi

  local base="master"
  if [[ "${1}" != "" ]]; then
    base="${1}"
  fi

  git checkout ${base} && \
  git pull && \
  git checkout ${branch} && \
  git rebase ${base}

}

function up {
  if [[ $(command -v yay) ]]; then
    yay -Syu --noconfirm
    yay -Yc --noconfirm
  elif [[ $(command -v apt) ]]; then
    sudo apt update
    sudo apt upgrade -y
    sudo apt autoremove -y
  fi
  flatpak update --assumeyes
  flatpak remove --unused --assumeyes
}

function rebase-off-master {
  current_branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
  if [ "${current_branch}" = "master" ]; then
    echo "Nothing to do"
    return
  fi

  git stash \
    && git checkout master \
    && git pull \
    && git checkout ${current_branch} \
    && git rebase master \
    && git stash pop
}

function is-hardlink {
  count=$(stat -c %h -- "${1}")
  if [ "${count}" -gt 1 ]; then
    echo "Yes.  There are ${count} links to this file."
  else
    echo "Nope.  This file is unique."
  fi
}
