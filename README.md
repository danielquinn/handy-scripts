# Handy Scripts

This is just a collection of tiny scripts I've written in the past that come in
handy on a regular basis.  They're all licensed under the GPL-3, so go nuts :-)


## backup

Crude Timemachine for Linux.  Choose a target directory and specify any
`--exclude=` entries and it'll backup everything incrementally.


## db

Make `.pgpass` even easier by extending it to include aliases.  Just change
your `.pgpass` file from this:

    some_host:5432:some_db_name:some_username:some_password

to this:

    # my-alias
    some_host:5432:some_db_name:some_username:some_password

The line immediately following a comment is attached to the alias in that
comment.  This way, you can now just log into your database like this:

    $ db my-alias


## highlight

Highlight multiple strings in different colours.  Just pipe the output of a
command through it and include the strings you'd like to see highlighted.
Note that this also supports standard regular expressions, and can handle up
to 9 separate strings.

```bash
$ /path/to/some/command | highlight.py alpha bravo charlie delta echo foxtrot
```


## combine-subtitles

A wrapper around ffmpeg to convert `.mp4` files and any number of `.srt` files
into one superior `.mkv` file:

    $ combine-subtitles my-video.mp4 --eng subs/english.srt --jpn subs/japanese.srt


## make-bootable-usb

Does what it says on the tin.  This is just so I don't have to remember all of
the special arguments you have to pass to `dd` to do this.


## memorymonitor

Gets a list of all processes and their memory usage, sorted in a
human-readable fashion


## pypublish

[Poetry](https://python-poetry.org/) is amazing, but it doesn't provide any
hooks for you to do simple things like (for example) modify a README file
before pushing it to PyPI.  So, if you're like me and have a repo with a bunch
of relative URLs in it pointing to other parts of the repo (diagrams, video
demos, etc), then that README will look broken on PyPI unless you patch the
file before publishing it.  That's what this does.

Where you once called `poetry build`, instead call this script and it will:

1. Go through your README and rewrite all of your relative URLs to be
   absolute ones pointing to the `master` repo.
2. Run `poetry build`
3. Restore your README.md file back to what it was.

> **NOTE**: This was written for GitLab, so if you're hoping to get this to work
with GitHub or some other service, you'll have to tweak it.


## q

An attempt at a simple "swiss army knife" tool.  The idea is to be able to
type `q <argument>` and have the script just *know* what I want.  So far it
supports:

* Unix timestamp to human-readable time
* Human-readable time to Unix time
* Simple math
* DNS lookups (forward and reverse)
* Radian/degree conversion


## rename

A crude replacement for the `rename` binary that comes with util-linux.  I use
this on my Synology, where util-linux is stragely unavailable.


## stabilise

Stabilise a video.  This is just a thing wrapper around a few ffmpeg commands.


## unfuck-mac

Apple devices like to impose The Apple Way of doing things on you, and as I was
compelled to use one years ago, I found this little script to be helpful.  It
removes that annoying behaviour in which Apple likes to re-purpose my function
keys for what it thinks are more useful purposes.

```bash
sudo unfuck-mac
```


## urldecode

Translates a URL with lots of encoding into something more readable

```bash
$ echo "I%20am%20the%20walrus" | urldecode
$ cat /path/to/file | urldecode
```


## urlencode

Translates a string into URL-encoded data

```bash
$ echo "I am the walrus" | urlencode
$ cat /path/to/file | urlencode
```


## wallpaper-changer

For GNOME, a simple way to rotate my desktop wallpaper every few minutes.
Copied almost entirely from [this handy comment](https://github.com/BigE/desk-changer/issues/62#issuecomment-480550963)
on an extension I used to use.


## switch-monitor

I work on a desktop or a laptop at home and this is a way to tell my monitor
through software that I want it to switch inputs.  Note that it requires
ddcutil to function.


## strip-bom

Windows has an annoying habit of attaching a special non-printed character
called a Byte Order Mark to files when it saves them as UTF-8.  This can
cause problems when interacting with Linux systems, so this is a little script
to strip that bit out.


## compress-video-for-shit-bandwidth

Just a one-line wrapper around a long ffmpeg command to make it easy to
remember how to do what it says on the tin.


## mksecret

Take a `.yaml` file containing Kubernetes secrets *without* the base64
encoding as input, and this will encode the secrets and write them to stdout.
A typical use-case is something like:

```shell
$ mksecret my-secrets.yaml | apply -f -
```

